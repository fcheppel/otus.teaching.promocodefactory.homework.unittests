﻿using System;
using System.Collections.Generic;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Helper
{
    public class PartnerBuilder
    {
        private readonly Partner _partner;

        public PartnerBuilder()
        {
            _partner = new Partner
            {
                Id = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8"),
                Name = "Суперигрушки",
                IsActive = true
            };
        }

        public Partner GetInactive()
        {
            _partner.IsActive = false;
            return _partner;
        }

        public Partner GetWithIssuedPromoCodes(Partner partner, int numberPromoCodes)
        {
            partner.NumberIssuedPromoCodes = numberPromoCodes;
            return partner;
        }

        public Partner GetWithOnlyOneActiveLimit()
        {
            _partner.PartnerLimits = new List<PartnerPromoCodeLimit>
            {
                new PartnerPromoCodeLimit
                {
                    Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                    CreateDate = new DateTime(2020, 07, 9),
                    EndDate = new DateTime(2020, 10, 9),
                    Limit = 100
                }
            };

            return _partner;
        }
    }
}
