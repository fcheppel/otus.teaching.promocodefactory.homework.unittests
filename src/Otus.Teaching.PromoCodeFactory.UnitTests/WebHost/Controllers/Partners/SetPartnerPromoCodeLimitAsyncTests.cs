﻿using System;
using System.Linq;
using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Helper;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _partnersController;
        private readonly PartnerBuilder _partnerBuilder = new PartnerBuilder();
        private readonly DateTime _endDate = new DateTime(DateTime.Today.Year, 12, 31);
        private const int Limit = 10;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }

        /// <summary>
        /// Если партнер не найден, то также нужно выдать ошибку 404
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerNotFound_NotFound()
        {
            var partnerId = Guid.Empty;
            _partnersRepositoryMock.Setup(r => r.GetByIdAsync(partnerId)).ReturnsAsync((Partner) null);

            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, new SetPartnerPromoCodeLimitRequest
            {
                EndDate = _endDate,
                Limit = Limit
            });

            result.Should().BeAssignableTo<NotFoundResult>();
        }

        /// <summary>
        /// Если партнер заблокирован, то есть поле IsActive=false в классе Partner,
        /// то также нужно выдать ошибку 400
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerNotActive_BadRequest()
        {
            var partnerId = Guid.Empty;
            var partner = _partnerBuilder.GetInactive();
            _partnersRepositoryMock.Setup(r => r.GetByIdAsync(partnerId)).ReturnsAsync(partner);

            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, new SetPartnerPromoCodeLimitRequest
            {
                EndDate = _endDate,
                Limit = Limit
            });

            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        /// <summary>
        /// Лимит должен быть больше 0;
        /// </summary>
        /// <param name="limit"></param>
        [Theory]
        [InlineData(5)]
        [InlineData(0)]
        [InlineData(-5)]
        public async void SetPartnerPromoCodeLimitAsync_ClearLimit_BadRequest(int limit)
        {
            var partnerId = Guid.Empty;
            var partner = _partnerBuilder.GetWithOnlyOneActiveLimit();
            _partnersRepositoryMock.Setup(r => r.GetByIdAsync(partnerId)).ReturnsAsync(partner);

            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, new SetPartnerPromoCodeLimitRequest
            {
                EndDate = _endDate,
                Limit = limit
            });

            if (limit < 0)
            {
                result.Should().BeAssignableTo<BadRequestObjectResult>();
            }
        }

        /// <summary>
        /// Если партнеру выставляется лимит, то мы должны обнулить количество промокодов,
        /// которые партнер выдал NumberIssuedPromoCodes,
        /// если лимит закончился, то количество не обнуляется;
        /// </summary>
        /// <param name="number"></param>
        [Theory]
        [InlineData(5)]
        [InlineData(0)]
        [InlineData(-5)]
        public async void SetPartnerPromoCodeLimitAsync_HasLimit_ShouldNumberIssuePromoCodeEqualsZero(int number)
        {
            var partner = _partnerBuilder.GetWithIssuedPromoCodes(_partnerBuilder.GetWithOnlyOneActiveLimit(), number);
            var partnerId = partner.Id;

            _partnersRepositoryMock.Setup(r => r.GetByIdAsync(partnerId)).ReturnsAsync(partner);
            await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, new SetPartnerPromoCodeLimitRequest
            {
                EndDate = _endDate,
                Limit = Limit
            });

            partner.NumberIssuedPromoCodes.Should().Be(0);
        }

        /// <summary>
        /// При установке лимита нужно отключить предыдущий лимит;
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_HasActiveLimit_ShouldSetCancelDate()
        {
            var partner = _partnerBuilder.GetWithOnlyOneActiveLimit();
            var partnerId = partner.Id;
            var activeLimitId = partner.PartnerLimits.FirstOrDefault()?.Id;
            _partnersRepositoryMock.Setup(r => r.GetByIdAsync(partnerId)).ReturnsAsync(partner);

            await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, new SetPartnerPromoCodeLimitRequest
            {
                EndDate = _endDate,
                Limit = Limit
            });

            var activeLimit = partner.PartnerLimits.FirstOrDefault(x => x.Id == activeLimitId);
            activeLimit?.CancelDate.Should().HaveValue();
        }

        /// <summary>
        /// Нужно убедиться, что сохранили новый лимит в базу данных (это нужно проверить Unit-тестом);
        /// </summary>
        /// <param name="limit"></param>
        [Theory]
        [InlineData(5)]
        [InlineData(3)]
        [InlineData(2)]
        public async void SetPartnerPromoCodeLimitAsync_NewLimitIsValid_NewLimitSavedInDb(int limit)
        {
            var partner = _partnerBuilder.GetWithOnlyOneActiveLimit();
            var partnerId = partner.Id;
            _partnersRepositoryMock.Setup(r => r.GetByIdAsync(partnerId)).ReturnsAsync(partner);

            await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, new SetPartnerPromoCodeLimitRequest
            {
                EndDate = _endDate,
                Limit = limit
            });

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);
            var newLimit = partner.PartnerLimits.FirstOrDefault(x => x.EndDate == _endDate && !x.CancelDate.HasValue && x.Limit == limit);
            newLimit.Should().NotBeNull();
        }
    }
}